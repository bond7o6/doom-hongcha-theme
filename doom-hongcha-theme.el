;;; doom-hongcha-theme.el --- The Hongcha Theme for use in Doom Emacs -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Copyright (C) 2021 Arlo Hobbs
;;
;; Author: Arlo Hobbs <https://gitlab.com/bond7o6>
;; Maintainer: Arlo Hobbs <arlohobbs@hotmail.com>
;; Created: November 18, 2021
;; Modified: November 18, 2021
;; Version: 0.0.1
;; Keywords: custom themes, faces
;; Homepage: https://gitlab.com/bond7o6/hongcha-theme
;; Package-Requires: ((emacs "25.1") (doom-themes "2.2.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  TODO
;;
;;  The Hongcha Theme for use in Doom Emacs.
;;  Intended to use with <<@SOMETHING@>>
;;
;;; Code:

(require 'doom-themes)


(defgroup doom-hongcha-theme nil
  "Options for the `doom-hongcha' theme."
  :group 'doom-themes)

(defcustom doom-hongcha-brighter-modeline nil
  "If non-nil, more vivid colours will be used to style the mode-line."
  :group 'hongcha-theme
  :type 'boolean)

(defcustom doom-hongcha-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colours."
  :group 'hongcha-theme
  :type 'boolean)

(defcustom doom-hongcha-comment-bg doom-hongcha-brighter-comments
  "If non-nil, comments will have a subtle, darker background.
Enhancing their legibility."
  :group 'hongcha-theme
  :type 'boolean)

(defcustom doom-hongcha-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'hongcha-theme
  :type '(choice integer boolean))

(def-doom-theme doom-hongcha
  "A dark theme to hopefully reduce migraines."



  ;; name         default
  ((bg          '("#0f0600")) ;; #612700 or #4f2000?
   (fg          '("#d9cbc3")) ;; #e6d7cf or #ede4df or #e7c9fb


   (bg-alt      '("#0a0400")) ;; #471d00?
   (fg-alt      '("#776559")) ;; #8d6aa5 or #776559
   (fg-emp      '("#E5BFFF")) ;; Provide emphasis for an in-text object via
                              ;; colour


   (base0       '("#050200"))
   ;; bg-alt
   ;; bg
   (base1       '("#231004")) ;; #1f0c00
   (base2       '("#40230f")) ;; #210d00
   (base3       '("#5e3a22")) ;; #270F00
   (base4       '("#7c563b")) ;; #503a2d
   (base5       '("#99755b")) ;; #776559 or fg-alt
   (base6       '("#b79883")) ;; #9f8f86
   (base7       '("#d5c0b1")) ;; #c6bab2
   ;; fg
   (base8       '("#f2ebe6"))


   (grey         '("#625b58"))
   (red          '("#ea3d54"));; "#ff6655" "red"          ))
   (orange       '("#da8548"));; "#dd8844" "brightred"    ))
   (green        '("#78bd65"));; "#99bb66" "green"        ))
   (teal         '("#4db5bd"));; "#44b9b1" "brightgreen"  ))
   (yellow       '("#ECBE7B"));; "#ECBE7B" "yellow"       ))
   (blue         '("#51afef"));; "#51afef" "brightblue"   )
   (dark-blue    '("#2257A0"));; "#2257A0" "blue"         ))
   (magenta      '("#c678dd"));; "#c678dd" "brightmagenta"))
   (violet       '("#a9a1e1"));; "#a9a1e1" "magenta"      ))
   (cyan         '("#46D9FF"));; "#46D9FF" "brightcyan"   ))
   (dark-cyan    '("#5699AF"));; "#5699AF" "cyan"         ))
   (harsh-red    '("#ee0000"))
   (harsh-green  '("#00ee00"))
   (harsh-yellow '("#eeaa00"))


   ;; face categories -- required for all themes
   (highlight      yellow)
   (vertical-bar   (doom-darken base4 0.2))
   (selection      red)
   (builtin        yellow)
   (comments       (doom-darken fg-alt 0.4))
   (doc-comments   (doom-lighten dark-cyan 0.25))
   (constants      orange)
   (functions      yellow)
   (keywords       red)
   (methods        yellow)
   (operators      green)
   (type           orange)
   (strings        (doom-darken dark-cyan 0.25))
   (variables      green)
   (numbers        orange)
   (region         `(,(doom-lighten (car bg-alt) 0.15)
                     ,@(doom-lighten (cdr base1) 0.35)))
   (error          harsh-red)
   (warning        harsh-yellow)
   (success        harsh-green)
   (vc-modified    orange)
   (vc-added       harsh-green)
   (vc-deleted     harsh-red)


   ;; custom categories
   (hidden     bg);; "black" "black"))
   (-modeline-bright doom-hongcha-brighter-modeline)
   (-modeline-pad
    (when doom-hongcha-padded-modeline
      (if (integerp doom-hongcha-padded-modeline) doom-hongcha-padded-modeline 4)))

   (modeline-fg     fg)
   (modeline-fg-alt base2)

   (modeline-bg
    (if -modeline-bright
        (doom-darken blue 0.475)
      `(,(doom-darken (car bg-alt) 0.35) ,@(cdr base0))))
   (modeline-bg-l
    (if -modeline-bright
        (doom-darken blue 0.45)
      `(,(doom-darken (car bg-alt) 0.1) ,@(cdr base0))))
   (modeline-bg-inactive   `(,(doom-darken (car bg) 0.03) ,@(cdr bg-alt)))
   (modeline-bg-inactive-l `(,(car bg-alt) ,@(cdr base1))))


  (
   ((line-number &override) :foreground base1)
   ((line-number-current-line &override) :foreground fg)
   ((font-lock-comment &override)
    :background (if doom-hongcha-comment-bg (doom-lighten bg 0.05)))
   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad
                             :color ,modeline-bg-inactive)))
   (mode-line-emphasis :foreground (if -modeline-bright base8 highlight))
   (fill-column-indicator :foreground base0)
   (hl-line :background base0)
   ;; (bold :foreground fg-emp :weight 'bold)


   ;;;; indent-guides
   ;; (highlight-indent-guides-stack-character-face :foreground blue)
   ;; (highlight-indent-guides-stack-even-face :foreground green)
   ;; (highlight-indent-guides-stack-odd-face :foreground yellow)
   (highlight-indent-guides-top-character-face :foreground base3)
   ;; (highlight-indent-guides-top-even-face :foreground green)
   ;; (highlight-indent-guides-top-odd-face :foreground yellow)
   (highlight-indent-guides-character-face :foreground base1)
   ;; (highlight-indent-guides-even-face :foreground green)
   ;; (highlight-indent-guides-odd-face :foreground yellow)
   ;;;; css-mode <built-in> / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)
   ;;;; doom-modeline
   (doom-modeline-bar         :background (if -modeline-bright modeline-bg
                                            highlight))
   (doom-modeline-buffer-file         :inherit 'mode-line-buffer-id :weight 'bold)
   (doom-modeline-buffer-path         :inherit 'mode-line-emphasis :weight 'bold)
   (doom-modeline-buffer-project-root :foreground green :weight 'bold)
   (doom-modeline-buffer-modified     :foreground red :weight 'bold)
   ;;;; elscreen
   (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
   ;;;; ivy
   (ivy-current-match   :background base3 :foreground orange)
   (ivy-posframe-cursor :background red :foreground base0)
   ;;;; js2-mode
   (js2-function-name     :forground yellow)
   (js2-function-param    :foreground blue)
   (js2-warning           :underline `(:style wave :color ,yellow))
   (js2-error             :underline `(:style wave :color ,red))
   (js2-external-variable :underline `(:style wave :color ,blue))
   (js2-jsdoc-tag         :background nil :foreground red)
   (js2-jsdoc-type        :background nil :foreground orange)
   (js2-jsdoc-value       :background nil :foreground blue)
   (js2-private-member    :background nil :foreground orange)
   (js2-object-property   :foreground fg)
   ;;;; LaTeX-mode
   (font-latex-math-face :foreground green)
   ;;;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   ((markdown-code-face &override) :background (doom-lighten base3 0.05))
   ;;;; markdown-mode
   (markdown-list-face       :foreground green)
   (markdown-pre-face        :foreground blue)
   (markdown-blockquote-face :inherit 'italic :foreground blue)
   (markdown-link-face       :inherit 'bold   :foreground orange)
   (markdown-header-face-1   :weight 'bold    :foreground magenta)
   (markdown-header-face-2   :weight 'bold    :foreground orange)
   (markdown-header-face-3   :weight 'bold    :foreground green)
   (markdown-header-face-4   :weight 'bold    :foreground yellow)
   (markdown-header-face-5   :weight 'bold    :foreground blue)
   (markdown-header-face-6   :weight 'bold    :foreground orange)
   ;;;; outline <built-in>
   (outline-1 :foreground magenta)
   (outline-2 :foreground orange)
   (outline-3 :foreground teal)
   (outline-4 :foreground green)
   (outline-5 :foreground magenta)
   (outline-6 :foreground orange)
   (outline-7 :foreground teal)
   (outline-8 :foreground green)
   ;;;; org <built-in>
   (org-link                  :foreground blue :underline t)
   (org-document-title        :foreground orange)
   (org-document-info-keyword :foreground comments)
   (org-meta-line             :foreground base6)
   (org-tag                   :foreground base6 :weight 'normal)
   (org-block                 :background (doom-darken bg 0.1 ) :extend t)
   (org-hide                  :foreground hidden)
   ;;;; org-agenda
   (org-agenda-date         :underline t
                            :overline t
                            :weight 'ultra-bold
                            :foreground violet
                            :height 1.25)
   (org-agenda-date-today   :underline t
                            :overline t
                            :weight 'ultra-bold
                            :foreground magenta
                            :height 1.33)
   (org-agenda-current-time :foreground fg-alt
                            :weight 'semi-bold
                            :slant 'italic)
   ;;;; org-super-agenda
   (org-super-agenda-header :foreground fg-emp
                            :overline nil
                            :underline t
                            :weight 'ultra-bold
                            :height 1.1)
   ;;;; show-paren
   (show-paren-match :foreground base0 :background base8 :weight 'ultra-bold)
   (show-paren-match-expression :foreground base0 :background base8 :weight 'ultra-bold)
   ;;;; rainbow-delimiters
   (rainbow-delimiters-depth-1-face  :foreground red)
   (rainbow-delimiters-depth-2-face  :foreground orange)
   (rainbow-delimiters-depth-3-face  :foreground green)
   (rainbow-delimiters-depth-4-face  :foreground cyan)
   (rainbow-delimiters-depth-5-face  :foreground blue)
   (rainbow-delimiters-depth-6-face  :foreground yellow)
   (rainbow-delimiters-depth-7-face  :foreground green)
   ;;;; rjsx-mode
   (rjsx-tag              :foreground fg)
   (rjsx-attr             :foreground orange :slant 'italic :weight 'medium)
   (rjsx-tag-bracket-face :foreground green)
   ;;;; solaire-mode
   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))
   ;;;; web-mode
   (web-mode-html-tag-face         :foreground fg :slant 'italic)
   ;;;; eshell
   (eshell-syntax-highlighting-shell-command-face :foreground green)
   (eshell-syntax-highlighting-invalid-face :foreground harsh-red)
   ;;;; magit
   (magit-section-heading :foreground magenta)
   (magit-diff-removed-highlight :foreground harsh-red
                                 :background (doom-darken red 0.80)
                                 :weight 'bold)
   ;;;; Gnus
   (gnus-emphasis-bold :foreground fg-emp)
   ;;;; ein
   (ein:basecell-input-area-face :background base1 :extend t))

  ;;;; Base theme variable overrides-
  ())


;;; doom-hongcha-theme.el ends here
